// Mailer
const nodemailer = require("nodemailer");
const handleBars = require('nodemailer-express-handlebars');

const MailerService = async (data) => {
    try {
        // Send e-mail using SMTP
        const smtpTransporter = nodemailer.createTransport({
            host: process.env.HOST_EMAIL_SES,
            port: process.env.PORT_EMAIL_SES,
            secure: false, // true for 465, false for other ports
            auth: {
                user: process.env.USER_EMAIL_SES,
                pass: process.env.USER_PASS_SES
            },
        });
        let handlebarOptions = {
            viewEngine: {
                extName: '.htm',
                partialsDir: 'public/email-templates/',
                layoutsDir: 'public/email-templates/',
                defaultLayout: data.layoutTemplate + '.htm'
            },
            viewPath: 'public/email-templates', // folder name
            extName: '.htm'
        };

        smtpTransporter.use('compile', handleBars(handlebarOptions));

        //console.log(data.from);

        const mailOptions = {
            from: data.message_title + ' <' + data.from + '>', // sender address ex : <test-sendemail@intercity.com>
            to: data.to,
            // text: 'This is some text',
            // html: '<p>This is some HTML</p>',
            // html: params.mensaje,
            subject: data.subject, //'First HTML Newsletter',
            template: data.layoutTemplate, // html body | file name
            context: data.context
        };
        smtpTransporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                // res.status(error.responseCode).send(error.response);
                // return error.response
            } else {
                console.log('Message sent: ' + info.response);
                // res.status(200).send(info);
                // return info
            }
        });
        return true;
    } catch (e) {
        console.log(e);
    }
};

module.exports = {
    MailerService
};
