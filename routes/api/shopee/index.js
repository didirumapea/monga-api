const express = require('express');
const router = express.Router();

// init controller
const {
    Callback,
    WebHook
} = require('~/src/controllers/shopee/integrationShopController');

// callback authentication shop
router.route('/callback')
    .get(Callback)
    .post(WebHook);

module.exports = router;