
var crypto = require('crypto');
const request = require('request');
const utf8 = require('utf8')

request.debug = true;

class LazadaApi {

    constructor(config) {
        if (config === null || config === undefined) {
            throw new Error("config required");
        }
        this.config = config;
    }

    getBaseUrl(key) {
        var baseUrl;

        switch (key) {
            case 'SG': //Singapore
                baseUrl = "https://api.lazada.sg/rest"
                break;
            case 'MY': //Malaysia
                baseUrl = "https://api.lazada.com.my/rest"
                break;
            case 'VN': //Vietnam
                baseUrl = "https://api.lazada.com.vn/rest"
                break;
            case 'TH': //Thailand
                baseUrl = "https://api.lazada.co.th/rest"
                break;
            case 'PH': //Philippines
                baseUrl = "https://api.lazada.com.ph/rest"
                break;
            case 'ID': //Indonesia
                baseUrl = "https://api.lazada.co.id/rest"
                break;
            default:
                baseUrl = "https://auth.lazada.com/rest"
                break;
        }
        return baseUrl;
    }

    buildAuthURL(token) {
        let authUrl = `https://auth.lazada.com/oauth/authorize?`;
        authUrl += `response_type=code`;
        authUrl += `&force_auth=true`;
        authUrl += `&redirect_uri=${this.config.redirect}?token=${token}`;
        authUrl += `&client_id=${this.config.client_id}`
        return authUrl;
    }

    generateSign(path, params) {
        var concatString = "";
        if (params) {
            params = this.sortKey(params);
            concatString = Object.keys(params).reduce(function (concatString, key) {
                return concatString.concat(key + params[key].toString());
            }, '')
        }

        var preSignString = path + concatString;
        var sign = crypto.createHmac('sha256', utf8.encode(this.config.secret_key))
            .update(preSignString)
            .digest('hex');
        return (utf8.decode(sign)).toUpperCase();
    }

    makeRequest(endpoint, query, data, method, callback = null) {
        const self = this;

        var sign = "";

        const cloneQuery = query === null || query === undefined ? {} : query;
        cloneQuery.app_key = `${self.config.client_id}`;
        cloneQuery.sign_method = "sha256";
        cloneQuery.timestamp = Math.floor(new Date() / 1000) + "000";

        sign = self.generateSign(endpoint, { ...query, ...data });

        cloneQuery.sign = sign;

        var baseUrl = "";
        if (this.config.country) {
            baseUrl = this.getBaseUrl(this.config.country.toUpperCase());
        } else {
            baseUrl = this.getBaseUrl();
        }

        const options = {
            baseUrl,
            uri: endpoint,
            method: method.toUpperCase() || "POST",
            headers: {},
            json: data,
            qs: { ...query, ...data }
        };

        self.verboseLog(`REQUEST: ${JSON.stringify(options)}\n`);

        const promise = new Promise(function (resolve, reject) {
            request(options, function (err, res, body) {
                if (err) {
                    self.verboseLog(`ERROR: ${err}`);
                    return reject(err);
                }

                self.verboseLog(`STATUS: ${res.statusCode}\n`);
                self.verboseLog(`HEADERS: ${JSON.stringify(res.headers)}\n`);
                self.verboseLog(`BODY: ${JSON.stringify(body)}\n`);

                return resolve({ body, res });
            });
        });

        if (callback === null) {
            return promise;
        }

        return promise
            .then(function ({ body, res }) {
                return callback(null, res, body);
            })
            .catch(function (err) {
                return callback(err, null, null);
            });

    }

    get(endpoint, query, data = null, callback = null) {
        return this.makeRequest(endpoint, query, data, "GET", callback);
    }

    post(endpoint, query, data = null, callback = null) {
        return this.makeRequest(endpoint, query, data, "POST", callback);
    }

    sortKey(data) {
        let sortedData = [];
        let sortedKey = Object.keys(data).sort(function (a, b) {
            let x = a.toLowerCase();
            let y = b.toLowerCase();
            if (x > y) { return 1; }
            if (x < y) { return -1; }
            return 0;
        });
        sortedKey.forEach(element => {
            sortedData[element] = data[element];
        });

        return sortedData;
    }

    verboseLog(msg) {
        if (this.config.verbose) {
            console.log(msg);
        }
    }

}

module.exports = LazadaApi;