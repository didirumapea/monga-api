const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Orders = new Schema({
    userID: { type: Schema.Types.ObjectId, ref: "users" },
    shopID: { type: Schema.Types.ObjectId, ref: "shopauthentication" },
    marketplace: { type: String, require: true },
    status: { type: Array },
    items: { type: Array },
    note: { type: String, default: null },
}, {
    versionKey: false,
})
Orders.plugin(timestamps);
module.exports = Orders;