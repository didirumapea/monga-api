const mongoose = require('mongoose');
const timestamps = require('mongoose-unix-timestamp-plugin');
const { Schema } = mongoose;

const Warehouse = new Schema({
    userID: { type: Schema.Types.ObjectId, ref: "user" },
    name: { type: String, required: true },
    address: { type: String, required: true },
    manager: { type: String, default: null },
    phone: { type: String, default: null }
}, {
    versionKey: false,
})
Warehouse.plugin(timestamps);
module.exports = Warehouse;