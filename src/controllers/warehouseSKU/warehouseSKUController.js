const { errorHandler } = require('~/src/config/errorHandler');
const warehouseSKUModel = require('~/src/models/warehouseSKUModel');

exports.listWarehouseSKU = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;

    // DATA SKU
    let payload = {
        userID: user.userID,
        ...request
    }

    var SKUData = await warehouseSKUModel.find(payload);
    if (!SKUData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: SKUData
    });
}

exports.createWarehouseSKU = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;
    var SKUData = [];

    // STORE SKU
    let payload = {
        userID: user.userID,
        ...request
    }
    try {
        const newSKU = new warehouseSKUModel(payload);
        await newSKU.save();

        // GET SKU DATA
        var SKUData = await warehouseSKUModel.find({ userID: user.userID });
        if (!SKUData) {
            return errorHandler.NotFound(res, "Data not found");
        }

    } catch (error) {
        let err = error.message;
        return errorHandler.BadRequest(res, err);
    }

    res.status(200).json({
        success: 1,
        data: SKUData
    });
}

exports.updateWarehouseSKU = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var params = req.params;
    var request = req.body;
    var SKUData = [];

    // UPDATE DOC COLLECTION
    const filter = { userID: user.userID, _id: params.id },
        update = request;
    var doc = await warehouseSKUModel.findOneAndUpdate(filter, update, {
        new: true, rawResult: true
    });

    if (!doc.lastErrorObject.updatedExisting) {
        return errorHandler.BadRequest(res, "Failed to update data.");
    }

    // GET SKU DATA
    var SKUData = await warehouseSKUModel.find({ userID: user.userID });
    if (!SKUData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: SKUData
    });
}

exports.deleteWarehouseSKU = async (req, res) => {
    try {
        // VARIABLE
        var request = req.params;
        let user = req.userData;
        var SKUData = [];

        // DELETE SHOP
        var doc = await warehouseSKUModel.deleteOne({ _id: request.id });
        if (doc.acknowledged && doc.deletedCount == 1) {

            // GET WAREHOUSE DATA
            var SKUData = await warehouseSKUModel.find({ userID: user.userID });
            if (!SKUData) {
                return errorHandler.NotFound(res, "Data not found");
            }

            res.status(200).json({
                success: 1,
                data: SKUData
            });

        } else {
            return errorHandler.BadRequest(res, "Record doesn't exist or already deleted")
        }

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}