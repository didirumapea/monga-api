const { errorHandler } = require('~/src/config/errorHandler');
const WarehouseModel = require('~/src/models/warehouseModel');

exports.listWarehouse = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;

    // DATA WAREHOUSE
    let payload = {
        userID: user.userID,
        ...request
    }

    var warehouseData = await WarehouseModel.find(payload);
    if (!warehouseData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: warehouseData
    });
}

exports.createWarehouse = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var request = req.body;
    var warehouseData = [];

    // STORE WAREHOUSE
    let payload = {
        userID: user.userID,
        ...request
    }
    try {
        const newWarehouse = new WarehouseModel(payload);
        await newWarehouse.save();

        // GET WAREHOUSE DATA
        var warehouseData = await WarehouseModel.find({ userID: user.userID });
        if (!warehouseData) {
            return errorHandler.NotFound(res, "Data not found");
        }

    } catch (error) {
        let err = error.message;
        return errorHandler.BadRequest(res, err);
    }

    res.status(200).json({
        success: 1,
        data: warehouseData
    });
}

exports.updateWarehouse = async (req, res) => {
    // VARIABLE
    const user = req.userData;
    var params = req.params;
    var request = req.body;
    var warehouseData = [];

    // UPDATE DOC COLLECTION
    const filter = { userID: user.userID, _id: params.id },
        update = request;
    var doc = await WarehouseModel.findOneAndUpdate(filter, update, {
        new: true, rawResult: true
    });

    if (!doc.lastErrorObject.updatedExisting) {
        return errorHandler.BadRequest(res, "Failed to update data.");
    }

    // GET WAREHOUSE DATA
    var warehouseData = await WarehouseModel.find({ userID: user.userID });
    if (!warehouseData) {
        return errorHandler.NotFound(res, "Data not found");
    }

    res.status(200).json({
        success: 1,
        data: warehouseData
    });
}

exports.deleteWarehouse = async (req, res) => {

    try {
        // VARIABLE
        var request = req.params;
        let user = req.userData;
        var warehouseData = [];

        // DELETE SHOP
        var doc = await WarehouseModel.deleteOne({ _id: request.id });
        if (doc.acknowledged && doc.deletedCount == 1) {

            // GET WAREHOUSE DATA
            var warehouseData = await WarehouseModel.find({ userID: user.userID });
            if (!warehouseData) {
                return errorHandler.NotFound(res, "Data not found");
            }

            res.status(200).json({
                success: 1,
                data: warehouseData
            });

        } else {
            return errorHandler.BadRequest(res, "Record doesn't exist or already deleted")
        }

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}