const { errorHandler } = require('~/src/config/errorHandler');
const moment = require("moment");

const { getAllAppList, getApp } = require('~/src/models/appListModel');
const { getShopbyId, getAllShopbyUserId, updateShopbyFilter, deleteShopbyId } = require('~/src/models/shopAuthenticationModel');
const LazadaApi = require('~/public/vendor/lazada/');
const LazadaAuth = require('~/public/vendor/lazada/authorization');
const ShopeeApi = require('~/public/vendor/shopee/shopee-core');
const ShopeeAuth = require('~/public/vendor/shopee/authorization');

exports.listMarketplace = async (req, res) => {
    try {

        // VARIABLE
        var token = req.headers.authorization;
        var baseUrl = req.hostname;
        var marketplaceList = [];
        var authUrl;

        // AMBIL DATA DARI APPLIST
        const apps = await getAllAppList();
        if (!apps) {
            return errorHandler.NotFound(res, "Platform not found");
        }

        apps.forEach(element => {
            // GENERATE URL-AUTH
            switch (element.marketplace) {
                case 'lazada':
                    const lazadaRequest = new LazadaApi({ redirect: element.apicallback, client_id: element.appid });
                    authUrl = lazadaRequest.buildAuthURL(token);
                    break;
                case 'shopee':
                    const shopeeRequest = new ShopeeApi({
                        isUAT: process.env.NODE_ENV == "dev" ? true : false,
                        partner_id: element.appid,
                        partner_key: element.appsecret,
                        redirect_uri: element.apicallback, // callback url when perform OAuth
                        webhook_url: element.apiwebhook,
                        verbose: process.env.NODE_ENV == "dev" ? true : false, // show more logs
                    });
                    authUrl = shopeeRequest.buildAuthURL(token);
                    break;
                default:
                    return errorHandler.NotFound(res, "Platform not found");
            }

            // KONSTRUK RESPONSE LIST-MARKETPLACE
            let payload = {
                images: `http://${baseUrl}:${process.env.HTTP_PORT}/images/assets/logo_${element.marketplace}.png`,
                marketplace: element.marketplace,
                url_auth: authUrl
            }

            marketplaceList.push(payload);
        });

        res.status(200).json({
            success: 1,
            data: marketplaceList
        });
    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}

exports.getListShop = async (req, res) => {
    try {
        // VARIABLE
        let user = req.userData;
        var shopData = [];

        // AMBIL SHOP DATA DARI SHOP-AUTH
        const shop = await getAllShopbyUserId({ userID: user.userID });
        if (!shop) {
            return errorHandler.NotFound(res, "Shop not found");
        }

        // KONSTRUK RESPONSE
        shop.forEach(element => {

            let marketplace = element.marketplace;
            let connected = element.expiredIn > moment().unix();
            let AuthorizedTime = moment.unix(element.updatedAt).format("DD-MM-YYYY HH:mm");

            let paylod = {
                id: element._id,
                marketplace: marketplace,
                shopID: element.shopID,
                name: element.shopName,
                country: element.country,
                connected: connected,
                chatStatus: false, // NANTI KETIKA MONGA CHAT JADI CEK OTORISASI CHAT
                AuthorizedTime: AuthorizedTime,
            }

            shopData.push(paylod);
        });

        res.status(200).json({
            success: 1,
            data: shopData
        });
    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

exports.resyncShop = async (req, res) => {
    try {
        // VARIABLE
        let user = req.userData;
        let request = req.body;
        var shopData = [];
        var accessToken,
            refreshToken,
            expiredIn,
            refresh_token,
            authData;

        // AMBIL DATA DARI APPLIST
        const apps = await getApp({ marketplace: request.marketplace });
        if (!apps) {
            return errorHandler.NotFound(res, "Platform not found");
        }

        // AMBIL SHOP DATA DARI APPLIST
        const shop = await getShopbyId({ userID: user.userID, shopID: request.shopID, marketplace: request.marketplace });
        if (!shop) {
            return errorHandler.NotFound(res, "Shop not found");
        }

        switch (request.marketplace) {
            case "shopee":
                let partner_id = apps.appid,
                    partner_key = apps.appsecret;
                refresh_token = shop.refreshToken;

                let ShopeeRequest = new ShopeeAuth({
                    partner_id,
                    partner_key,
                    verbose: true
                });

                // CALL API REFRESH TOKEN
                authData = await ShopeeRequest.refreshAccessToken(refresh_token, shop.shopID);
                if (authData.error != '') {
                    return errorHandler.BadRequest(res, authData.message ? authData.message : authData.msg); //NANTINYA KEMUNGKINAN ADA TRANSLATE RESPONSE
                }

                // SET VARIABLE
                accessToken = authData.access_token;
                refreshToken = authData.refresh_token;
                expiredIn = moment().add(authData.expire_in, 'second').format('X');

                break;
            case "lazada":
                let client_id = apps.appid,
                    secret_key = apps.appsecret;
                refresh_token = shop.refreshToken;

                let LazadaRequest = new LazadaAuth({
                    client_id,
                    secret_key,
                    verbose: true
                });

                // CALL API REFRESH TOKEN
                authData = await LazadaRequest.refreshAccessToken(refresh_token);
                if (authData.code != 0) {
                    return errorHandler.BadRequest(res, authData.message); //NANTINYA KEMUNGKINAN ADA TRANSLATE RESPONSE
                }

                // SET VARIABLE
                accessToken = authData.access_token;
                refreshToken = authData.refresh_token;
                expiredIn = moment().add(authData.expires_in, 'second').format('X');

                break;
            default:
                return errorHandler.BadRequest(res, "Platform not found");
        }

        // UPDATE DOC COLLECTION
        const filter = { 'userID': user.userID, 'shopID': request.shopID, 'marketplace': request.marketplace },
            update = { accessToken, refreshToken, expiredIn };

        var doc = await updateShopbyFilter(filter, update);
        if (!doc.lastErrorObject.updatedExisting) {
            return errorHandler.BadRequest(res, "Failed to update data.");
        }

        // AMBIL SHOP DATA DARI SHOP-AUTH
        const shopList = await getAllShopbyUserId({ userID: user.userID });
        if (!shopList) {
            return errorHandler.NotFound(res, "Shop not found");
        }

        // KONSTRUK RESPONSE
        shopList.forEach(element => {

            let marketplace = element.marketplace,
                connected = element.expiredIn > moment().unix(),
                AuthorizedTime = moment.unix(element.updatedAt).format("DD-MM-YYYY HH:mm");

            let paylod = {
                id: element._id,
                marketplace: marketplace,
                shopID: element.shopID,
                name: element.shopName,
                country: element.country,
                connected: connected,
                chatStatus: false, // NANTI KETIKA MONGA CHAT JADI CEK OTORISASI CHAT
                AuthorizedTime: AuthorizedTime,
            }

            shopData.push(paylod);
        });

        res.status(200).json({
            success: 1,
            data: shopData
        });

    } catch (error) {
        errorHandler.UnHandler(res, error);
    }
}

exports.deleteShop = async (req, res) => {
    try {
        // VARIABLE
        var request = req.params;
        let user = req.userData;
        var shopData = [];

        // DELETE SHOP
        var doc = await deleteShopbyId(request.id);
        if (doc != 1) {
            return errorHandler.BadRequest(res, "Record doesn't exist or already deleted")
        }

        // AMBIL SHOP DATA DARI SHOP-AUTH
        const shopList = await getAllShopbyUserId({ userID: user.userID });
        if (!shopList) {
            return errorHandler.NotFound(res, "Shop not found");
        }

        // KONSTRUK RESPONSE
        shopList.forEach(element => {

            let marketplace = element.marketplace,
                connected = element.expiredIn > moment().unix(),
                AuthorizedTime = moment.unix(element.updatedAt).format("DD-MM-YYYY HH:mm");

            let paylod = {
                id: element._id,
                marketplace: marketplace,
                shopID: element.shopID,
                name: element.shopName,
                country: element.country,
                connected: connected,
                chatStatus: false, // NANTI KETIKA MONGA CHAT JADI CEK OTORISASI CHAT
                AuthorizedTime: AuthorizedTime,
            }

            shopData.push(paylod);
        });

        res.status(200).json({
            success: 1,
            data: shopData
        });

    } catch (error) {
        return errorHandler.UnHandler(res, error)
    }
}