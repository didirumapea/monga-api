const { errorHandler } = require('~/src/config/errorHandler');
const AppListModel = require('~/src/models/appListModel');
const ShopAuthenticationModel = require('~/src/models/shopAuthenticationModel');
const ShopeeApi = require('~/public/vendor/shopee/shopee-core');
const ShopeeOrders = require('~/public/vendor/shopee/orders');

// GET ORDER LIST
exports.getOrders = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getOrders(req, res)

        // const sign = shopeeApi.isValidSignature("/api/v2/order/get_order_list", timestamp); //sign private API
        // const time_from = moment(req.query.time_from).format("X"); // default yesterday;
        // const time_to = moment(req.query.time_to).format("X"); //default now()
        // if (req.body.order_status) {
        //
        //
        // }

        // REQUEST API SHOPEE ORDER
        // const payloadOrder = {
        //     sign,
        //     time_range_field: 'create_time',
        //     time_from,
        //     time_to,
        //     order_status: req.query.order_status,
        //     page_size: req.query.page_size,
        //     shop_id: shopData.shopID,
        //     access_token: shopData.accessToken,
        // }
        //
        // shopeeApi.get('/api/v2/order/get_order_list', payloadOrder, {}, (err, response, body) => {
        //     if (err) {
        //         console.log(err)
        //     }
        //     // RETURN DATA
        //     res.status(200).json({
        //         success: 1,
        //         data: body
        //     });
        //     // console.log(body)
        // });

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// GET DETAIL ORDER
exports.getDetailOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getDetailOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// GET SHIPMENT LIST
exports.getShipmentList = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getShipmentList(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// SPLIT ORDER
exports.splitOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.splitOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// UNSPLIT ORDER
exports.unsplitOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.unsplitOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// CANCEL ORDER
exports.cancelOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.cancelOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// SET NOTE
exports.setNote = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.setNote(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// HANDLE BUYER CANCELLATION
exports.handleBuyerCancellation = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.handleBuyerCancellation(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// ADD INVOICE DATA
exports.addInvoiceData = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.addInvoiceData(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// GET PENDING BUYER INVOICE ORDER LIST
exports.getPendingBuyerInvoiceOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getPendingBuyerInvoiceOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// GET SHIPPING PARAMETER
exports.getShippingParameter = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getShippingParameter(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// SHIP ORDER
exports.shipOrder = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.shipOrder(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}

// GET TRACKIN NUMBER
exports.getTrackingNumber = async (req, res) => {
    try {
        // SETUP TIMESTAMP
        const timestamp = Math.floor(new Date() / 1000);

        // FIND APP LIST
        const appData = await AppListModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // FIND SHOP AUTH
        const shopData = await ShopAuthenticationModel.findOne({ marketplace: "shopee" }).sort({ createdAt: -1 });
        // PAYLOADS
        const payloadApi = {
            isUAT: true,
            partner_id: appData.appid,
            partner_key: appData.appsecret,
            access_token: shopData.accessToken,
            redirect_uri: appData.apicallback, // callback url when perform OAuth
            webhook_url: appData.apiwebhook,
            shop_id: shopData.shopID,
            verbose: true, // show more logs
        }

        let shopeeApi = new ShopeeApi(payloadApi);

        let shopeeOrders = new ShopeeOrders({
            ...payloadApi,
            api: shopeeApi,
            timestamp: timestamp
        })

        shopeeOrders.getTrackingNumber(req, res)

    } catch (err) {
        errorHandler.UnHandler(res, err);
    }
}
