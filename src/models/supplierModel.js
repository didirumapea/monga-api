const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const SupplierSchema = require('~/src/databases/schema/supplierSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const SupplierModel = mongoose.model('supplier', SupplierSchema);

module.exports = SupplierModel;