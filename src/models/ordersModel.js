const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const OrdersSchema = require('~/src/databases/schema/ordersSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const OrdersModel = mongoose.model('orders', OrdersSchema);

async function createOrders(query) {
    const create = new OrdersModel(query);
    return await create.save();
}

async function getAllOrders(query) {
    const select = await OrdersModel.find(query).populate('shopID');
    return select;
}

async function getOrders(query) {
    const select = await OrdersModel.findOne(query);
    return select;
}

async function getLastCreate(query) {
    const select = await OrdersModel.findOne(query, ['createdAt']).sort({ createdAt: 1 });
    return select;
}

module.exports = { createOrders, getAllOrders, getOrders, getLastCreate };