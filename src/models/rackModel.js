const mongoose = require('mongoose');

// const mongoosePaginate = require('mongoose-paginate-v2');
// const mongooseAgregatePaginate = require('mongoose-aggregate-paginate-v2');
const RackSchema = require('~/src/databases/schema/rackSchema');

// UserSchema.plugin(mongoosePaginate);
// UserSchema.plugin(mongooseAgregatePaginate);

const RackModel = mongoose.model('rack', RackSchema);

module.exports = RackModel;